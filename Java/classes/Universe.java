import java.awt.Color;
import java.util.LinkedList;

public class Universe {
	
	LinkedList<Card> allCards = new LinkedList<Card>();
	LinkedList<Ressource> ressources = new LinkedList<Ressource>();
	
	public Universe() {
		Ressource energy = new Ressource("Energy");
		ressources.add(energy);
		Ressource oxygen = new Ressource("Oxygen");
		ressources.add(oxygen);
		Ressource science = new Ressource("Science");
		ressources.add(science);
		
		Card newCard = new Card();
		
		Color energyColor = new Color (255,50,50);
		Color oxygenColor = new Color (0,200,0);
		Color scienceColor = new Color (100,100,255);
		Color otherColor = new Color (255,255,100);
		
		Color energyLightColor = new Color (255,100,100);
		Color oxygenLightColor = new Color (100,200,100);
		Color scienceLightColor = new Color (200,200,255);
		Color otherLightColor = new Color (255,255,200);
		
		Color energyBorderColor = new Color (255,0,0);
		Color oxygenBorderColor = new Color (0,255,0);
		Color scienceBorderColor = new Color (0,0,255);
		Color otherBorderColor = new Color (255,255,0);
		
		
		newCard = new Card("Rover");
		newCard.fillColor = energyLightColor;
		newCard.borderColor = energyBorderColor;
		newCard.cost.add(new RessourceContainer(energy,1));
		newCard.actions.add(new CardAction("attack",2));
		newCard.description = "Attack 2 HP";
		allCards.add(newCard);
		
		newCard = new Card("Trooper");
		newCard.fillColor = energyLightColor;
		newCard.borderColor = energyBorderColor;
		newCard.cost.add(new RessourceContainer(energy,3));
		newCard.actions.add(new CardAction("attack",5));
		newCard.description = "Attack 5 HP";
		allCards.add(newCard);
		
		newCard = new Card("Cruiser");
		newCard.fillColor = energyLightColor;
		newCard.borderColor = energyBorderColor;
		newCard.cost.add(new RessourceContainer(energy,14));
		newCard.actions.add(new CardAction("attack",18));
		newCard.description = "Attack 18 HP";
		allCards.add(newCard);
		
		newCard = new Card("Laser");
		newCard.fillColor = energyLightColor;
		newCard.borderColor = energyBorderColor;
		newCard.cost.add(new RessourceContainer(energy,16));
		newCard.actions.add(new CardAction("attack",20));
		newCard.description = "Attack 20 HP";
		allCards.add(newCard);
		
		newCard = new Card("Spy");
		newCard.fillColor = otherLightColor;
		newCard.borderColor = energyBorderColor;
		newCard.cost.add(new RessourceContainer(energy,4));
		newCard.actions.add(new CardAction("spy"));
		newCard.description = "View enemy cards";
		allCards.add(newCard);
		
		newCard = new Card("Squadron");
		newCard.fillColor = energyLightColor;
		newCard.borderColor = energyBorderColor;
		newCard.cost.add(new RessourceContainer(energy,7));
		newCard.actions.add(new CardAction("attack",9));
		newCard.description = "Attack 9 HP";
		allCards.add(newCard);
		
		newCard = new Card("Terrorism");
		newCard.fillColor = energyLightColor;
		newCard.borderColor = energyBorderColor;
		newCard.cost.add(new RessourceContainer(energy,20));
		newCard.actions.add(new CardAction("attackCity",15));
		newCard.description = "Attack 15 HP, ignore shield";
		allCards.add(newCard);
		
		newCard = new Card("Jet Fighter");
		newCard.fillColor = energyLightColor;
		newCard.borderColor = energyBorderColor;
		newCard.cost.add(new RessourceContainer(energy,10));
		newCard.actions.add(new CardAction("attack",10));
		newCard.description = "Attack 10 HP";
		allCards.add(newCard);
		
		newCard = new Card("Smugglers");
		newCard.fillColor = otherLightColor;
		newCard.borderColor = energyBorderColor;
		newCard.cost.add(new RessourceContainer(energy,16));
		newCard.actions.add(new CardAction("smugglers",8));
		newCard.description = "Steal 8 of each ressource";
		allCards.add(newCard);
		
		newCard = new Card("Tokamak");
		newCard.fillColor = energyColor;
		newCard.borderColor = energyBorderColor;
		newCard.cost.add(new RessourceContainer(energy,8));
		newCard.actions.add(new CardAction("build",energy));
		newCard.description = "Produces Energy";
		allCards.add(newCard);
		
		newCard = new Card("Generator");
		newCard.fillColor = oxygenLightColor;
		newCard.borderColor = energyBorderColor;
		newCard.cost.add(new RessourceContainer(energy,7));
		newCard.actions.add(new CardAction("shield",12));
		newCard.description = "Plus 12 to shield";
		allCards.add(newCard);
		
		newCard = new Card("Force Field");
		newCard.fillColor = otherLightColor;
		newCard.borderColor = energyBorderColor;
		newCard.cost.add(new RessourceContainer(energy,8));
		newCard.actions.add(new CardAction("forceField"));
		newCard.description = "block enemy production for 1 turn";
		allCards.add(newCard);
		
		newCard = new Card("Conscription");
		newCard.fillColor = energyLightColor;
		newCard.borderColor = energyBorderColor;
		newCard.cost.add(new RessourceContainer(energy,1));
		newCard.actions.add(new CardAction("allProduce",energy));
		newCard.description = "All production goes to energy";
		allCards.add(newCard);
		
		
		/////////////////////////////////
		
		newCard = new Card("Lab");
		newCard.fillColor = scienceColor;
		newCard.borderColor = scienceBorderColor;
		newCard.cost.add(new RessourceContainer(science,8));
		newCard.actions.add(new CardAction("build",science));
		newCard.description = "Produces Science";
		allCards.add(newCard);
		
		newCard = new Card("Plasma Gun");
		newCard.fillColor = energyLightColor;
		newCard.borderColor = scienceBorderColor;
		newCard.cost.add(new RessourceContainer(science,20));
		newCard.actions.add(new CardAction("attack",22));
		newCard.description = "Attack 22 HP";
		allCards.add(newCard);
		
		newCard = new Card("Gravitational Wave");
		newCard.fillColor = energyLightColor;
		newCard.borderColor = scienceBorderColor;
		newCard.cost.add(new RessourceContainer(science,24));
		newCard.actions.add(new CardAction("attack",27));
		newCard.description = "Attack 27 HP";
		allCards.add(newCard);
		
		newCard = new Card("Congress Center");
		newCard.fillColor = oxygenLightColor;
		newCard.borderColor = scienceBorderColor;
		newCard.cost.add(new RessourceContainer(science,18));
		newCard.actions.add(new CardAction("build",22));
		newCard.description = "Plus 22 to colony";
		allCards.add(newCard);
		
		newCard = new Card("Tesla Tower");
		newCard.fillColor = oxygenLightColor;
		newCard.borderColor = scienceBorderColor;
		newCard.cost.add(new RessourceContainer(science,14));
		newCard.actions.add(new CardAction("shield",20));
		newCard.description = "Plus 20 to shield";
		allCards.add(newCard);
		
		newCard = new Card("Treason");
		newCard.fillColor = oxygenLightColor;
		newCard.borderColor = scienceBorderColor;
		newCard.cost.add(new RessourceContainer(science,10));
		newCard.actions.add(new CardAction("treason"));
		newCard.description = "Next enemy attack will do zero damage";
		allCards.add(newCard);
		
		newCard = new Card("Precision");
		newCard.fillColor = energyLightColor;
		newCard.borderColor = scienceBorderColor;
		newCard.cost.add(new RessourceContainer(science,15));
		newCard.actions.add(new CardAction("precision"));
		newCard.description = "Next attack will do double damage";
		allCards.add(newCard);
		
		newCard = new Card("Fusion");
		newCard.fillColor = scienceLightColor;
		newCard.borderColor = scienceBorderColor;
		newCard.cost.add(new RessourceContainer(science,5));
		newCard.actions.add(new CardAction("produce",energy,8));
		newCard.description = "Plus 8 energy";
		allCards.add(newCard);
		
		newCard = new Card("Peer Review");
		newCard.fillColor = scienceLightColor;
		newCard.borderColor = scienceBorderColor;
		newCard.cost.add(new RessourceContainer(science,5));
		newCard.actions.add(new CardAction("produce",science,8));
		newCard.description = "Plus 8 science";
		allCards.add(newCard);
		
		newCard = new Card("Abiogenesis");
		newCard.fillColor = scienceLightColor;
		newCard.borderColor = scienceBorderColor;
		newCard.cost.add(new RessourceContainer(science,5));
		newCard.actions.add(new CardAction("produce",oxygen,8));
		newCard.description = "Plus 8 oxygen";
		allCards.add(newCard);
		
		newCard = new Card("Waste");
		newCard.fillColor = scienceLightColor;
		newCard.borderColor = scienceBorderColor;
		newCard.cost.add(new RessourceContainer(science,5));
		newCard.actions.add(new CardAction("remove",energy,8));
		newCard.description = "Remove 8 energy to opponent";
		allCards.add(newCard);
		
		newCard = new Card("Amnesia");
		newCard.fillColor = scienceLightColor;
		newCard.borderColor = scienceBorderColor;
		newCard.cost.add(new RessourceContainer(science,5));
		newCard.actions.add(new CardAction("remove",science,8));
		newCard.description = "Remove 8 science to opponent";
		allCards.add(newCard);
		
		newCard = new Card("Leak");
		newCard.fillColor = scienceLightColor;
		newCard.borderColor = scienceBorderColor;
		newCard.cost.add(new RessourceContainer(science,5));
		newCard.actions.add(new CardAction("remove",oxygen,8));
		newCard.description = "Remove 8 oxygen to opponent";
		allCards.add(newCard);
		
		newCard = new Card("Darwin Day");
		newCard.fillColor = scienceLightColor;
		newCard.borderColor = scienceBorderColor;
		newCard.cost.add(new RessourceContainer(science,1));
		newCard.actions.add(new CardAction("allProduce",science));
		newCard.description = "All production goes to science";
		allCards.add(newCard);
		
		////////////////////////////////////////////////////////
		
		newCard = new Card("Greenhouse");
		newCard.fillColor = oxygenColor;
		newCard.borderColor = oxygenBorderColor;
		newCard.cost.add(new RessourceContainer(oxygen,8));
		newCard.actions.add(new CardAction("build",oxygen));
		newCard.description = "Produces oxygen";
		allCards.add(newCard);
		
		newCard = new Card("Settlers");
		newCard.fillColor = oxygenLightColor;
		newCard.borderColor = oxygenBorderColor;
		newCard.cost.add(new RessourceContainer(oxygen,5));
		newCard.actions.add(new CardAction("build",5));
		newCard.description = "Plus 5 to colony";
		allCards.add(newCard);
		
		newCard = new Card("Suburbs");
		newCard.fillColor = oxygenLightColor;
		newCard.borderColor = oxygenBorderColor;
		newCard.cost.add(new RessourceContainer(oxygen,10));
		newCard.actions.add(new CardAction("build",10));
		newCard.description = "Plus 10 to colony";
		allCards.add(newCard);
		
		newCard = new Card("Residential");
		newCard.fillColor = oxygenLightColor;
		newCard.borderColor = oxygenBorderColor;
		newCard.cost.add(new RessourceContainer(oxygen,12));
		newCard.actions.add(new CardAction("build",15));
		newCard.description = "Plus 15 to colony";
		allCards.add(newCard);
		
		newCard = new Card("Urban Complex");
		newCard.fillColor = oxygenLightColor;
		newCard.borderColor = oxygenBorderColor;
		newCard.cost.add(new RessourceContainer(oxygen,25));
		newCard.actions.add(new CardAction("build",30));
		newCard.description = "Plus 30 to colony";
		allCards.add(newCard);
		
		newCard = new Card("Wind Mill");
		newCard.fillColor = oxygenLightColor;
		newCard.borderColor = oxygenBorderColor;
		newCard.cost.add(new RessourceContainer(oxygen,4));
		newCard.actions.add(new CardAction("shield",6));
		newCard.description = "Plus 6 to shield";
		allCards.add(newCard);
		
		newCard = new Card("Expansion");
		newCard.fillColor = otherColor;
		newCard.borderColor = oxygenBorderColor;
		newCard.cost.add(new RessourceContainer(oxygen,30));
		newCard.actions.add(new CardAction("build",energy));
		newCard.actions.add(new CardAction("build",oxygen));
		newCard.actions.add(new CardAction("build",science));
		newCard.description = "Plus 1 Forest, 1 Power Plant and 1 Lab";
		allCards.add(newCard);
		
		newCard = new Card("Geothermics");
		newCard.fillColor = oxygenLightColor;
		newCard.borderColor = oxygenBorderColor;
		newCard.cost.add(new RessourceContainer(oxygen,5));
		newCard.actions.add(new CardAction("shield",9));
		newCard.description = "Plus 9 to shield";
		allCards.add(newCard);
		
		newCard = new Card("Poison Gaz");
		newCard.fillColor = energyLightColor;
		newCard.borderColor = oxygenBorderColor;
		newCard.cost.add(new RessourceContainer(oxygen,10));
		newCard.actions.add(new CardAction("attack",12));
		newCard.description = "Attack 12 HP";
		allCards.add(newCard);
		
		newCard = new Card("Flamethrower");
		newCard.fillColor = energyLightColor;
		newCard.borderColor = oxygenBorderColor;
		newCard.cost.add(new RessourceContainer(oxygen,7));
		newCard.actions.add(new CardAction("attack",9));
		newCard.description = "Attack 9 HP";
		allCards.add(newCard);
		
		newCard = new Card("Selective Immigration");
		newCard.fillColor = otherLightColor;
		newCard.borderColor = oxygenBorderColor;
		newCard.cost.add(new RessourceContainer(oxygen,10));
		newCard.actions.add(new CardAction("selectiveImmigration",6));
		newCard.description = "Takes 6 population points to eneny and bring them to colony";
		allCards.add(newCard);
		
		newCard = new Card("Recycling");
		newCard.fillColor = scienceLightColor;
		newCard.borderColor = oxygenBorderColor;
		newCard.cost.add(new RessourceContainer(oxygen,1));
		newCard.actions.add(new CardAction("allProduce",oxygen));
		newCard.description = "All production goes to oxygen";
		allCards.add(newCard);
		

		newCard = new Card("Fossil Fuel");
		newCard.fillColor = oxygenLightColor;
		newCard.borderColor = oxygenBorderColor;
		newCard.cost.add(new RessourceContainer(oxygen,14));
		newCard.actions.add(new CardAction("shield",20));
		newCard.description = "Plus 20 to shield";
		allCards.add(newCard);
		
		newCard = new Card("Pacifism");
		newCard.fillColor = otherLightColor;
		newCard.borderColor = oxygenBorderColor;
		newCard.cost.add(new RessourceContainer(oxygen,3));
		newCard.actions.add(new CardAction("pacifism",4));
		newCard.description = "Convert 4 shield points to population";
		allCards.add(newCard);
		
		
		
		
		
		
		
		
	}

}
