import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Mock_GUI extends JFrame implements ActionListener {
	
	JPanel main;
	Battle battle;
	JPanel console;
	
	private JCheckBox[] discardBoxes = new JCheckBox[8];
	private JButton discardButton;

	public Mock_GUI() {
		
		setTitle("Mars Colonies");
		setSize(800, 600);
		setLocationRelativeTo(null);               
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		console = new JPanel();
		console.setBorder(new LineBorder(Color.BLACK));
		console.setLayout(new BoxLayout (console, BoxLayout.PAGE_AXIS)); 
		
		clearConsole();
		log("*******************************************************");
		log("* MARS COLONIES");
		log("* ........................................................................................");
		log("* Cliquer sur PLAY pour joueur une carte.");
		log("* ");
		log("* Cliquer sur INFO pour lire les effets d'une carte.");
		log("* ");
		log("* Pour se d�fausser de 1 � 3 cartes, selectionner leur");
		log("* case 'Discard' puis cliquer sur 'DISCARD SELECTED'.");
		log("* ");
		log("* Le premier qui atteint 100 HP a gagn�.");
		log("* Le premier qui atteint 0 HP a perdu.");
		log("* L'ordinateur joue al�atoirement.");
		log("* ");
		log("*******************************************************");
	}
	
	public void clearConsole() {
		console.removeAll();
		console.add(new JLabel("Console"));
		
	}
	
	public void log(String text) {
		
		console.add(new JLabel(text));

	
	}
	
	public void showBattleState(Battle battle) {
		
		this.battle = battle;
		//removeAll();
		if (main != null) {
			remove(main);
		}
		
		main = new JPanel();
		main.setLayout(new BorderLayout ()); 
		main.add(console,BorderLayout.CENTER);
		
		
		JPanel coloniesStats = new JPanel();
		coloniesStats.setLayout(new BorderLayout ()); 
		JPanel player1ColonyStats = getColonyStats(battle.player1);
		coloniesStats.add(player1ColonyStats,BorderLayout.PAGE_START);
		JPanel player2ColonyStats = getColonyStats(battle.player2);
		coloniesStats.add(player2ColonyStats,BorderLayout.PAGE_END);
		main.add(coloniesStats,BorderLayout.PAGE_START);
		
		JPanel coloniesDetails1 =   getColonyDetails(battle.player1);
		main.add(coloniesDetails1,BorderLayout.WEST);
		
		JPanel coloniesDetails2 =   getColonyDetails(battle.player2);
		main.add(coloniesDetails2,BorderLayout.EAST);
		
		JPanel footer = new JPanel();
		footer.setLayout(new BorderLayout ()); 
		
		
		JPanel player1Cards =   getPlayerCards(battle.player1);
		footer.add(player1Cards,BorderLayout.CENTER);
		
		JPanel DiscardPanel = new JPanel();
		discardButton = new JButton("DISCARD SELECTED");
		DiscardPanel.add(discardButton);
		discardButton.addActionListener(this);
		footer.add(DiscardPanel,BorderLayout.EAST);
		
		main.add(footer,BorderLayout.SOUTH);

		add(main);
		revalidate();
	}
	
	public JPanel getPlayerCards(Player player){
		
		JPanel cardsPanel = new JPanel();
		cardsPanel.setLayout(new BoxLayout (cardsPanel, BoxLayout.LINE_AXIS)); 
		
		for (int i = 0; i<8; i++) {
			
			Card card = player.hand[i];
			
			
		
			
			JPanel cardJP =  new JPanel();
			cardJP.setLayout(new BoxLayout (cardJP, BoxLayout.PAGE_AXIS)); 
			
			

			cardJP.setBorder(new LineBorder(card.borderColor));
			cardJP.setBackground(card.fillColor);
			
			System.out.print(card.fillColor);
			
			
			
			cardJP.add(new JLabel(card.name));
			cardJP.add(new JLabel("------------------"));
			cardJP.add(new JLabel("Cost : "));
			
			for (int j=0; j<card.cost.size();j++) {
				RessourceContainer ressourceContainer = card.cost.get(j);
				cardJP.add(new JLabel(ressourceContainer.quantity+" " + ressourceContainer.type.name));
				
			}
			
			cardJP.add(new JLabel("------------------"));
			
		
			JButton playButton = new JButton("PLAY "+i);
			cardJP.add(playButton);
			playButton.addActionListener(battle);
			playButton.setEnabled(battle.player1.canPay(card));
			
			JButton infoButton = new JButton("INFO "+i);
			cardJP.add(infoButton);
			infoButton.addActionListener(battle);
			
			JCheckBox discardCheckbox = new JCheckBox("Discard");
			cardJP.add(discardCheckbox);
			discardCheckbox.addActionListener(this);
			discardBoxes[i] = discardCheckbox;
			
			cardsPanel.add(cardJP);
			
		}
		return cardsPanel;
		
	}
	
	public JPanel getColonyDetails(Player player){
		
		
		
		JPanel detailsPanel = new JPanel();
		detailsPanel.setLayout(new BoxLayout (detailsPanel, BoxLayout.PAGE_AXIS)); 
		detailsPanel.add(new JLabel(player.name+" :"));
		
		
		detailsPanel.add(new JLabel(" "));
		detailsPanel.add(new JLabel("Shield buildings :"));
		for (int i=0; i<player.colony.shieldBuildings.size(); i++) {
			String text = player.colony.shieldBuildings.get(i).name;
			text += " HP=  "+ player.colony.shieldBuildings.get(i).hitPoints;
			detailsPanel.add(BorderCell(text));
		}
		
		detailsPanel.add(new JLabel(" "));
		detailsPanel.add(new JLabel("Structural buildings :"));
		for (int i=0; i<player.colony.structuralBuildings.size(); i++) {
			String text = player.colony.structuralBuildings.get(i).name;
			text += " HP=  "+ player.colony.structuralBuildings.get(i).hitPoints;
			detailsPanel.add(BorderCell(text));
		}
		
		detailsPanel.add(new JLabel(" "));
		detailsPanel.add(new JLabel("Production buildings :"));
		for (int i=0; i<player.colony.productionBuildings.size(); i++) {
			String text = player.colony.productionBuildings.get(i).name;
			text += " produces 1 "+ player.colony.productionBuildings.get(i).ressource.name;
			detailsPanel.add(BorderCell(text));
		}
		
	
		return detailsPanel;
		
	}
	
	private JLabel BorderCell(String text) {
		JLabel newLabel = new JLabel(text);
		newLabel.setBorder(new LineBorder(Color.BLACK));
		return newLabel;
	}
	
	public JPanel getColonyStats(Player player){
		
		JPanel statsPanel = new JPanel();
			
		statsPanel.add(new JLabel("Colony " + player.name+" :"));
		statsPanel.add(BorderCell(" HP=" + player.colony.getHitPoints()+" "));
		statsPanel.add(BorderCell(" Shield=" + player.colony.getShieldPoints()+" "));
		
		for (int i=0; i<player.universe.ressources.size();i++) {
			for (int j=0; j<player.colony.ressources.size(); j++) {
				if(player.colony.ressources.get(j).type.name == player.universe.ressources.get(i).name) {
					String text = " " + player.colony.ressources.get(j).type.name+": ";		
					text += player.colony.ressources.get(j).quantity+"(+";
					int count = 0;
					for (int k=0; k<player.colony.productionBuildings.size(); k++) {
						if (player.colony.productionBuildings.get(k).ressource.name == player.universe.ressources.get(i).name) {
							count ++;
						}
					}
					text +=count+") ";
					
					statsPanel.add(BorderCell(text));
				}
			}
		}
	
		
///////////////////////////////////////////////////////////////////////
		return statsPanel;
		
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		
		if (battle.winner() == null) {
			if (ae.getSource() == discardButton ) {
				clearConsole();
				log("**********************************************");
				for (int i = 0; i<8; i++) {
					
					if (discardBoxes[i].isSelected()==true) {
						battle.player1.discard(i);
						log ("* " +battle.player1.name + " discarded one card");
					}
				}
				battle.nextTurn();
				
			}
			
			
			int count = 0;
			
			for (int i = 0; i<8; i++) {
				//System.out.println(ae.getSource() == discardBoxes[i] );
				//System.out.println(discardBoxes[i].isSelected());
				
				if ( discardBoxes[i].isSelected() ) {
					count ++;
				}
			}
			//System.out.println(count );
			if (count>=3) {
				
				for (int i = 0; i<8; i++) {
					
					discardBoxes[i].setEnabled(discardBoxes[i].isSelected());
				}
				
			}
			else {
				
				for (int i = 0; i<8; i++) {
					
					discardBoxes[i].setEnabled(true);
				}
				
			}
			
		}
		
		
	}

}
