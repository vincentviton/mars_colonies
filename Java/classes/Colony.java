import java.util.LinkedList;

public class Colony {

	LinkedList<StructuralBuilding> shieldBuildings = new LinkedList<StructuralBuilding>();
	LinkedList<StructuralBuilding> structuralBuildings = new LinkedList<StructuralBuilding>();
	LinkedList<ProductionBuilding> productionBuildings = new LinkedList<ProductionBuilding>();
	LinkedList<RessourceContainer> ressources = new LinkedList<RessourceContainer>();
	
	double takeHitMultiplyer = 1;
	double attacktMultiplyer = 1;
	double removeMultiplyer = 1;
	double produceMultiplyer = 1;
	Ressource monopolyRessource = null;
	public Colony() {
		
	}
	
	public void addProductionBuilding (ProductionBuilding building) {
		productionBuildings.add(building);
	}
	
	public void addStructuralBuilding (StructuralBuilding building) {
		structuralBuildings.add(building);
	}
	
	public void addShieldBuilding (StructuralBuilding building) {
		shieldBuildings.add(building);
	}
	
	public void produce() {	
		
		if (monopolyRessource != null) {
			for (int j=0; j<ressources.size();j++) {
				if (ressources.get(j).type == monopolyRessource) {
					ressources.get(j).quantity += productionBuildings.size()* produceMultiplyer;
					monopolyRessource= null;
				}
			}
		}
		
		else {
			for (int i =0; i<productionBuildings.size();i++) {
				for (int j=0; j<ressources.size();j++) {
					if (ressources.get(j).type.name == productionBuildings.get(i).ressource.name ) {
						ressources.get(j).quantity += produceMultiplyer ;
					}
				}
			}	
		}	
		
		produceMultiplyer = 1;
	}
		
	
	
	public void takeDirectHit(int amount) {
		
		 removeHitPoints(structuralBuildings, (int)(amount*takeHitMultiplyer));
		 if (takeHitMultiplyer==0) {System.out.println("Attack failed because of Treason");}
		 if (takeHitMultiplyer==2) {System.out.println("Double damage because of Precision");}
		 takeHitMultiplyer = 1;
}
	
	public void takeHit(int amount) {
		//amount *= takeHitMultiplyer;
			amount = removeHitPoints(shieldBuildings, (int)(amount*takeHitMultiplyer));
			takeDirectHit(amount);
			
	}
	
	public int removeRessource(Ressource ressource, int quantity) {System.out. println(quantity);
		quantity *= removeMultiplyer;
		
		for (int i=0; i<ressources.size();i++) {		
			if (ressources.get(i).type.name == ressource.name ) {
				
				if (ressources.get(i).quantity < quantity) {
					quantity = ressources.get(i).quantity;
					
				}
				
					ressources.get(i).quantity -= quantity;
				
			}
		}
		removeMultiplyer = 1;
		return quantity;
		
	}
	
public void addRessource(Ressource ressource, int quantity) {
		
		for (int i=0; i<ressources.size();i++) {		
			if (ressources.get(i).type.name == ressource.name ) {
				ressources.get(i).quantity += quantity;
			}
		}
		
	
		
	}
	
	
	private int removeHitPoints (LinkedList<StructuralBuilding> buildingList, int amount) {
		
		
		
		
		while (amount>0 && buildingList.size()>0) {
			
			int min = 999999;
			StructuralBuilding smallest = new StructuralBuilding();
			
			for (int i=0;i<buildingList.size();i++) { // get the building with less hitPoints
				if ( buildingList.get(i).hitPoints<min) {
					smallest = buildingList.get(i);
					min= buildingList.get(i).hitPoints;
				} 
			}
			
			if (amount >= smallest.hitPoints ) {
				
				amount-= smallest.hitPoints;
				buildingList.remove(smallest);		
			}
			
			else
			{
				smallest.hitPoints -= amount;
				amount = 0;
			}
			System.out.println(amount);
		}
		
		return amount;
	}
	
	public int getHitPoints() {
		return getPoints(structuralBuildings);
	}
	
	public int getShieldPoints() {
		return getPoints(shieldBuildings);
	}
	
	private int getPoints(LinkedList<StructuralBuilding> buildingList) {
		
		int hitPoints = 0;
		
		for (int i=0;i<buildingList.size();i++) {
			hitPoints+= buildingList.get(i).hitPoints;
		}
		return hitPoints;
	}
	

}
