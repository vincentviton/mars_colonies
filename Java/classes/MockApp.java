

public class MockApp {

	public static void main(String[] args) {
		Universe universe = new Universe();
		Player player1 = new Player(universe);
		Player player2 = new Player(universe);
		
		player1.addRandomDeck(40);
		player2.addRandomDeck(40);
		
		new Battle (player1,player2);
	}

}
