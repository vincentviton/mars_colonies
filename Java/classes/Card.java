import java.awt.Color;
import java.util.LinkedList;

public class Card {
	String name;
	String description;
	LinkedList<CardAction> actions = new LinkedList<CardAction>();
	LinkedList<RessourceContainer> cost = new LinkedList<RessourceContainer>();
	Color fillColor;
	Color borderColor;
	public Card() {
		
	}
	
	public Card(String name) {
		this.name = name;
	}
	
	public Card copy() {
		Card newCard = new Card();
		newCard.name = this.name;
		newCard.description = this.description;
		newCard.actions = this.actions;
		newCard.cost = this.cost;
		newCard.fillColor = this.fillColor;
		newCard.borderColor = this.borderColor;
		return newCard;
	}

}
