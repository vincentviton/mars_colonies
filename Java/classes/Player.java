import java.util.LinkedList;

public class Player {
int nVictory;
int nDefeat;
int nDraw;
String name;
Universe universe;
Colony colony;
Deck currentDeck;
Deck discardDeck;
Card[] hand;
LinkedList<Card> unlockedCards = new LinkedList<Card>();
LinkedList<Deck> decks = new LinkedList<Deck>();
	
	
	public Player(Universe universe) {
		this.universe = universe;
		hand = new Card[8];
		discardDeck = new Deck();
		
	}
	
	public void addRandomDeck(int size) {
		
		Deck deck = new Deck();
		deck.name = "Random Deck";
		
		for (int i=0; i<size; i++) {
			int random = (int) (Math.random()*universe.allCards.size());
			Card newCard = universe.allCards.get(random).copy();
			deck.cards.add(newCard);
			
		}
		
		decks.add(deck);
		currentDeck = deck;
		
		//System.out.println(deck.cards);
	}
	
	public Boolean canPay( Card card) {
		
		Boolean canPay = true;
				
		for (int i =0; i<colony.ressources.size();i++) {
			for (int j =0; j<card.cost.size();j++) {
				if (card.cost.get(j).type.name == colony.ressources.get(i).type.name) {		
					if (card.cost.get(j).quantity > colony.ressources.get(i).quantity) {
						//gui.log("Not enough resources to play this card");
						canPay = false;
					}
				}
			}
		}
				
		return canPay;
				
	}
	
	public Card discard(int i) {
		Card newCard  = currentDeck.draw();
		hand[i] = newCard;
		currentDeck.cards.remove(newCard);
		discardDeck.cards.add(newCard);
		return newCard;
	}
	
	public void draw() {
		for (int i=0; i< 8 ; i++) {
			if (hand[i]==null) {
				Card newCard  = currentDeck.draw();
				hand[i] = newCard;
				currentDeck.cards.remove(newCard);
				discardDeck.cards.add(newCard);
				
			}
		}
	}
}
