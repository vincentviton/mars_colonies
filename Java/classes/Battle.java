import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;

public class Battle implements ActionListener {
	Player player1;
	Player player2;
	Mock_GUI gui;
	
	

	public Battle() {
		// TODO Auto-generated constructor stub
	}
	public Battle(Player p1, Player p2) {
	player1 = p1;
	player2 = p2;
	
	player1.name = "You";
	player2.name = "Computer";
	
	initiateColony(player1);
	initiateColony(player2);
	
	player1.draw();
	player2.draw();
	
	 gui = new Mock_GUI();
	gui.showBattleState(this);
	
	/*System.out.println("Cartes joueur 1");
	//System.out.println(player2.hand[7].name);
	for (int i=0; i<8; i++) {
		System.out.println("*************************");
		System.out.println("Carte "+i);
		Card card = player1.hand[i]; 
		System.out.println(card.name);
		for (int j=0; j<card.cost.size(); j++) {
			RessourceContainer ressourceContainer = card.cost.get(j);
			System.out.println("Cout : "+ ressourceContainer.quantity + " " + ressourceContainer.type.name);
		}
	}
	
	
	playCard(player1,0);
	
	for (int i=0; i<8; i++) {
		System.out.println("*************************");
		System.out.println("Carte "+i);
		Card card = player1.hand[i]; 
		System.out.println(card.name);
		for (int j=0; j<card.cost.size(); j++) {
			RessourceContainer ressourceContainer = card.cost.get(j);
			System.out.println("Cout : "+ ressourceContainer.quantity + " " + ressourceContainer.type.name);
		}
	}*/
	
	
	}
	
	public void nextTurn() {
		//System.out.println(player1.colony.getHitPoints()<=0);
		
		//System.out.println(winner());
		
		
		
		if (winner()==null) {
			player2.colony.produce();
			aIRandomCardChoice(player2);
			if(winner()==null) {
				player1.colony.produce();
				
			}
			gui.showBattleState(this);	
			
		}
		
		
		
		
		
	
		
			
	}
	
	public Player winner() {
		Player win = null;
		if (player1.colony.getHitPoints()<=0) win = player2;
		else  if (player2.colony.getHitPoints()<=0) win = player1;
		else  if (player1.colony.getHitPoints()>=100) win = player1;
		else  if (player2.colony.getHitPoints()>=100) win = player2;
		
		if (win != null) {
			gui.log("**************************************");
			gui.log(win.name+" won the game");
			gui.showBattleState(this);	
		}
		
		return win;
		
	}
	
	
	public void aIRandomCardChoice(Player player) {
		
		
		LinkedList<Integer> playableCardsIndices = new LinkedList<Integer>();
		for (int i=0; i<8;i++) {
			if (player.canPay( player.hand[i])) {
				playableCardsIndices.add(i);
			}
		}
		int random=0;
		if (playableCardsIndices.size()==0) {
			random = (int) (Math.random()*8);
			player.hand[random]=null;
			player.draw();
			gui.log(player.name+" discarded 1 card");
		}
		else {
			random = (int) (Math.random()*playableCardsIndices.size());
			playCard(player,playableCardsIndices.get(random));
			
		}
		
	}
	
	public void playCard(Player player, int handIndex) {
		
		Card card = player.hand[handIndex];
		
		
		
		if (player.canPay(card) ) {
			gui.log("**********************************************");
			gui.log("* "+player.name + " played " + card.name);
			gui.log("* ("+ card.description +")");
			payForCard (player,card);
			playCardActions(player,card);
			player.hand[handIndex]=null;
			player.draw();
			
			
			////////////////////////////////////////////
			
			
		}
		
		
		
	}
	

	
	/*public Boolean canPlay(Player player, Card card) {
				
		Boolean canPlay = true;
				
		for (int i =0; i<player.colony.ressources.size();i++) {
			for (int j =0; j<card.cost.size();j++) {
				if (card.cost.get(j).type.name == player.colony.ressources.get(i).type.name) {		
					if (card.cost.get(j).quantity > player.colony.ressources.get(i).quantity) {
						//gui.log("Not enough resources to play this card");
						canPlay = false;
					}
				}
			}
		}
				
		return canPlay;
				
	}*/
	
	public void payForCard(Player player, Card card) {
		
	
				
		for (int i =0; i<player.colony.ressources.size();i++) {
			for (int j =0; j<card.cost.size();j++) {
				if (card.cost.get(j).type.name == player.colony.ressources.get(i).type.name) {		
					
						player.colony.ressources.get(i).quantity -= card.cost.get(j).quantity ;
				}
			}
		}	
	}
	
	public void playCardActions(Player player,Card card) {
		
		Player opponent = player2;
		if (player == player2 ) opponent = player1;
		
		for (int i=0; i<card.actions.size(); i++) {
			CardAction action =  card.actions.get(i);
			int amount = action.amount;
			Ressource ressource = action.ressourceType;
			
			switch (action.actionType) {
			
			case "attack" : 
				opponent.colony.takeHit(amount);
				break;
				
			case "attackCity" : 
				opponent.colony.takeDirectHit(amount);
				break;
				
			case "build" : 
				if (ressource == null) { // structural buildings have no associated resource
					player.colony.addStructuralBuilding(new StructuralBuilding(card.name,amount));
				}
				else {
					player.colony.addProductionBuilding( new ProductionBuilding(card.name,ressource));
				}
				
				break;
				
			case "shield" : 
				
					player.colony.addShieldBuilding(new StructuralBuilding(card.name,amount));
				
				
				break;
				
			case "produce" : 
				
				player.colony.addRessource(ressource,amount);
			
			
			break;
			
			case "remove" : 
				
				opponent.colony.removeRessource(ressource,amount);
			
			
			break;
			
			case "spy" : 
				gui.log("Enemy's cards :");
				for (int j=0; j<8;j++) {
					gui.log(opponent.hand[j].name);
				}

			break;
			
			case "smugglers" : // steals at most 8 units per resource
				
				for (int j=0; j<player.colony.ressources.size();j++) {
					for (int k=0; k<opponent.colony.ressources.size();k++) {
						if (opponent.colony.ressources.get(k).type.name == player.colony.ressources.get(j).type.name) {
							int amount2 = opponent.colony.removeRessource(opponent.colony.ressources.get(k).type, amount);
							System.out.println(amount2);
							player.colony.addRessource(player.colony.ressources.get(j).type,amount2);
						}
					}

				}

			break;
			
			case "forceField" : 
				
				opponent.colony.produceMultiplyer = 0;
			
			
			break;  
			case "allProduce" : 
				
				player.colony.monopolyRessource = ressource;
			
			
			break;
			
			case "treason" : 
				
				player.colony.takeHitMultiplyer = 0;
			
			
			break;
			
			case "precision" : 
				
				opponent.colony.takeHitMultiplyer = 2;
			
			
			break;
			
			case"selectiveImmigration" :
				
				int amount2 = (int) (amount*opponent.colony.takeHitMultiplyer);
				
				if (opponent.colony.getHitPoints() < amount2) {
					amount2 = opponent.colony.getHitPoints();
				}
				
				opponent.colony.takeDirectHit(amount);
				
				player.colony.addStructuralBuilding(new StructuralBuilding("Migrants Camp",amount2));
				
				break;
				
			case"pacifism" :
				if (player.colony.getShieldPoints() < amount) {
					amount = player.colony.getShieldPoints();
					
					
					
				}
				
				player.colony.takeHit(amount);
				player.colony.addStructuralBuilding(new StructuralBuilding("Hippy Community",amount));
				
				break;
			
			
			
			

			}
			
		
		}
	}
	
	public void initiateColony(Player player) {
		player.colony = new Colony();
		player.colony.addProductionBuilding(new ProductionBuilding ("Tokamak",player.universe.ressources.get(0)));
		player.colony.addProductionBuilding(new ProductionBuilding ("Tokamak",player.universe.ressources.get(0)));
		player.colony.addProductionBuilding(new ProductionBuilding ("GreenHouse",player.universe.ressources.get(1)));
		player.colony.addProductionBuilding(new ProductionBuilding ("GreenHouse",player.universe.ressources.get(1)));
		player.colony.addProductionBuilding(new ProductionBuilding ("Lab",player.universe.ressources.get(2)));
		player.colony.addProductionBuilding(new ProductionBuilding ("Lab",player.universe.ressources.get(2)));
		
		player.colony.addStructuralBuilding(new StructuralBuilding("Tower",24));
		player.colony.addShieldBuilding(new StructuralBuilding("Generator",10));
		
		player.colony.ressources.add(new RessourceContainer(player.universe.ressources.get(0),5));
		player.colony.ressources.add(new RessourceContainer(player.universe.ressources.get(1),5));
		player.colony.ressources.add(new RessourceContainer(player.universe.ressources.get(2),5));
		
	}
	
	public void displayCardDescription(Card card) {
		gui.log("**********************************************");
		gui.log("* Card info '" + card.name +"':" );
		gui.log(card.description);
		gui.log("**********************************************");
		gui.showBattleState(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
	if (winner() == null) {
		
		gui.clearConsole();
		
		String text = ((JButton) arg0.getSource()).getText();
		
		switch (text) {
		
			case "PLAY 0" : 
			playCard(player1,0);
			nextTurn();
			break;
			
			case "PLAY 1" : 
			playCard(player1,1);
			nextTurn();
			break;
			
			case "PLAY 2" :
			playCard(player1,2);
			nextTurn();
			break;
			
			case "PLAY 3" : 
			 playCard(player1,3);
			 nextTurn();
			break;
			
			case "PLAY 4" :  
			playCard(player1,4);
			nextTurn();
			break;
			
			case "PLAY 5" : 
			 playCard(player1,5);
			 nextTurn();
			break;
			
			case "PLAY 6" :  
			playCard(player1,6);
			nextTurn();
			break;
			
			case "PLAY 7" :  
			playCard(player1,7);
			nextTurn();
			break;
			
			case "INFO 0" : displayCardDescription(player1.hand[0]);
			break;
			
			case "INFO 1" :displayCardDescription(player1.hand[1]);
			break;
			
			case "INFO 2" : displayCardDescription(player1.hand[2]);
			break;
			
			case "INFO 3" : displayCardDescription(player1.hand[3]);
			break;
			
			case "INFO 4" : displayCardDescription(player1.hand[4]);
			break;
			
			case "INFO 5" : displayCardDescription(player1.hand[5]);;
			break;
			
			case "INFO 6" : displayCardDescription(player1.hand[6]);
			break;
			
			case "INFO 7" : displayCardDescription(player1.hand[7]);
			break;
			
			}
		
		}
	}
}
