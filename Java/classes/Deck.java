import java.util.LinkedList;

public class Deck {
	String name;
	LinkedList<Card> cards = new LinkedList<Card>();
	
	private LinkedList<Card> discardStack = new LinkedList<Card>();
	public Deck() {
		// TODO Auto-generated constructor stub
	}
	
	public Card draw() {
		
					if(cards.size()==0) { // refill deck when empty
						
						for (int j = 0 ; j < discardStack.size() ; j ++ ) {
							cards.add(discardStack.get(j));
						}
						
					}
		
			
					int random = (int) (Math.random()*cards.size());
					Card newCard  = cards.get(random);
					
					cards.remove(newCard);
					discardStack.add(newCard);
					
				
					return newCard;
			
		}


}
