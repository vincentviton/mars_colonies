import java.util.LinkedList;

public class CardAction {
	
	String actionType;
	Ressource ressourceType;
	int amount;
	LinkedList<Integer> args = new LinkedList<Integer>();

	public CardAction() {
		
	}
	
	public CardAction(String actionType) {
		this.actionType = actionType;
	}
	
	public CardAction(String actionType, int amount) {
		this.actionType = actionType;
		this.amount = amount;
	}
	
	public CardAction(String actionType, Ressource ressourceType) {
		this.actionType = actionType;
		this.ressourceType = ressourceType;
	}
	
	public CardAction(String actionType, Ressource ressourceType, int amount) {
		this.actionType = actionType;
		this.amount = amount;
		this.ressourceType = ressourceType;
	}
	
	
	
	

}
